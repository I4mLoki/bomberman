#include "CustomGameViewportClient.h"

bool UCustomGameViewportClient::InputKey(const FInputKeyEventArgs& EventArgs)
{
	if (IgnoreInput() || EventArgs.IsGamepad() || EventArgs.Key.IsMouseButton())
		return Super::InputKey(EventArgs);

	UEngine* const Engine = GetOuterUEngine();
	int32 const NumPlayers = Engine ? Engine->GetNumGamePlayers(this) : 0;
	bool RetVal = Super::InputKey(EventArgs);

	for (int32 i = 1; i < NumPlayers; i++)
	{
		FInputKeyEventArgs Input(EventArgs);
		Input.ControllerId = i;

		RetVal = Super::InputKey(Input);
	}

	return RetVal;
}
