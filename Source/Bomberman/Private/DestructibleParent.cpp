#include "DestructibleParent.h"

#include "Bomberman/Bomberman.h"

ADestructibleParent::ADestructibleParent()
{
	PrimaryActorTick.bCanEverTick = false;

	TriggerBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box Detector"));
	SetRootComponent(TriggerBoxComponent);
}

void ADestructibleParent::BeginPlay()
{
	Super::BeginPlay();

	TriggerBoxComponent->SetBoxExtent(FVector(BoxSize, BoxSize, BoxSize), false);

	TriggerBoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	TriggerBoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	TriggerBoxComponent->SetCollisionResponseToChannel(TRACE_BOMB, ECR_Overlap);
}

void ADestructibleParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADestructibleParent::SetData(float Size, AMapManager* MapManager)
{
	BoxSize = Size;
	MapInstance = MapManager;
}

void ADestructibleParent::DestroyObject()
{
	Destroy();
}


