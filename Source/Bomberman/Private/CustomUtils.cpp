#include "CustomUtils.h"

bool UCustomUtils::CheckProbability(float Chance)
{
	float Offset = FMath::FRandRange(0, 65535);
	float RndTemp = FMath::FRandRange(0, 2);
	float DoubleChance = Chance * 2;
	float p = 2 - DoubleChance;

	return RndTemp > 2 - DoubleChance && RndTemp < FMath::Clamp(FMath::Fmod(Offset, p) + DoubleChance, 0.f, 2.f);
}
