#include "MapManager.h"
#include "CustomGameMode.h"
#include "CustomUtils.h"
#include "DestructibleTile.h"
#include "Bomberman/Bomberman.h"
#include "Kismet/GameplayStatics.h"

AMapManager::AMapManager()
{
	PrimaryActorTick.bCanEverTick = false;
	AttachComponents();
}

void AMapManager::BeginPlay()
{
	Super::BeginPlay();

	PopulateMap();
	SetMapMeshes();
	SetMap();
	CreateDestructibleTiles();
}

void AMapManager::OnConstruction(const FTransform& Transform)
{
	PopulateMap();
	SetMapMeshes();
}

void AMapManager::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMapManager::PopulateMap()
{
	TileList.Reset();
	TileList.Init(FTile(), MapColumns * MapRows);

	for (int i = 0; i < MapColumns; ++i)
	{
		for (int j = 0; j < MapRows; ++j)
		{
			if (i == 0 || j == 0 || i == MapColumns - 1 || j == MapRows - 1)
				TileList[ColumnRowPosition(i, j)].TileType = ETileType::EWallTile;
			else if (i % 2 == 0 && j % 2 == 0 && UCustomUtils::CheckProbability(SpawnNonDestructibleTileChance))
				TileList[ColumnRowPosition(i, j)].TileType = ETileType::ENonDestructibleTile;
			else if (UCustomUtils::CheckProbability(SpawnDestructibleTileChance))
				TileList[ColumnRowPosition(i, j)].TileType = ETileType::EDestructibleTile;
		}
	}

	FVector Player1TileStart;
	FVector Player2TileStart;

	Player1TileStart.X = FMath::RandRange(1, MapRows / 3);
	Player1TileStart.Y = FMath::RandRange(1, MapColumns / 3);

	Player2TileStart.X = FMath::Clamp(MapRows - Player1TileStart.X, 1.f, MapRows - 2.f);
	Player2TileStart.Y = FMath::Clamp(MapColumns - Player1TileStart.Y, 1.f, MapColumns - 2.f);

	DeleteTilesNextToPlayer(Player1TileStart.Y, Player1TileStart.X);
	DeleteTilesNextToPlayer(Player2TileStart.Y, Player2TileStart.X);

	TileList[ColumnRowPosition(Player1TileStart.Y, Player1TileStart.X)].TileType = ETileType::EPlayerTile;
	TileList[ColumnRowPosition(Player2TileStart.Y, Player2TileStart.X)].TileType = ETileType::EPlayerTile;

	PlayersSpawnTiles.Reset();
}

void AMapManager::DeleteTilesNextToPlayer(const int Column, const int Row)
{
	TileList[ColumnRowPosition(Column, FMath::Clamp(Row - 1, 1, MapRows - 2))].TileType = ETileType::EEmptyTile;
	TileList[ColumnRowPosition(Column, FMath::Clamp(Row + 1, 1, MapRows - 2))].TileType = ETileType::EEmptyTile;
	TileList[ColumnRowPosition(FMath::Clamp(Column + 1, 1, MapColumns - 2), Row)].TileType = ETileType::EEmptyTile;
	TileList[ColumnRowPosition(FMath::Clamp(Column - 1, 1, MapColumns - 2), Row)].TileType = ETileType::EEmptyTile;
}

void AMapManager::CreateDestructibleTiles()
{
	for (int i = 0; i < TileList.Num(); ++i)
	{
		if (TileList[i].TileType == ETileType::EDestructibleTile)
		{
			FTransform DestructibleTransform = FTransform(GetActorRotation(), TileList[i].TilePosition, GetActorScale3D());
			ADestructibleTile* DestructibleTile = Cast<ADestructibleTile>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, DestructibleTileTemplate, DestructibleTransform));

			if (DestructibleTile)
			{
				DestructibleTile->SetData(TileSize / 2, this);
				UGameplayStatics::FinishSpawningActor(DestructibleTile, DestructibleTransform);
			}
		}
	}
}

void AMapManager::SetMapMeshes()
{
	WallTilesComponentInstance->ClearInstances();
	DestructibleTilesComponentInstance->ClearInstances();
	NonDestructibleTilesComponentInstance->ClearInstances();
	FloorTilesComponentInstance->ClearInstances();

	if (DestructibleTileMesh && NonDestructibleTileMesh && FloorTileMesh)
	{
		TileSize = FloorTileMesh->GetBounds().BoxExtent.X * 2;

		WallTilesComponentInstance->SetStaticMesh(WallTileMesh);
		DestructibleTilesComponentInstance->SetStaticMesh(DestructibleTileMesh);
		NonDestructibleTilesComponentInstance->SetStaticMesh(NonDestructibleTileMesh);
		FloorTilesComponentInstance->SetStaticMesh(FloorTileMesh);

		float RowsOffset = TileSize * (MapRows / 2);
		float ColumnsOffset = -TileSize * (MapColumns / 2);

		for (int i = 0; i < MapColumns; ++i)
		{
			for (int j = 0; j < MapRows; ++j)
			{
				int Instance = -1;

				FVector TilePosition = GetActorLocation() + GetActorForwardVector() * RowsOffset + GetActorRightVector() * ColumnsOffset;

				FloorTilesComponentInstance->AddInstanceWorldSpace(FTransform(GetActorRotation(), TilePosition, GetActorScale3D()));

				FTransform TileTransform = FTransform(GetActorRotation(), TilePosition + FVector(0.f, 0.f, TileSize), GetActorScale3D());

				switch (TileList[ColumnRowPosition(i, j)].TileType)
				{
				case ETileType::EWallTile:
					{
						Instance = WallTilesComponentInstance->AddInstanceWorldSpace(TileTransform);
						break;
					}
				case ETileType::EDestructibleTile:
					{
						Instance = DestructibleTilesComponentInstance->AddInstanceWorldSpace(TileTransform);
						break;
					}
				case ETileType::ENonDestructibleTile:
					{
						Instance = NonDestructibleTilesComponentInstance->AddInstanceWorldSpace(TileTransform);
						break;
					}
				case ETileType::EPlayerTile:
					{
						PlayersSpawnTiles.Add(TileTransform.GetLocation());
						break;
					}
				}

				TileList[ColumnRowPosition(i, j)].TilePosition = TileTransform.GetLocation();
				TileList[ColumnRowPosition(i, j)].MeshInstance = Instance;

				RowsOffset -= TileSize;
			}

			RowsOffset = TileSize * (MapRows / 2);
			ColumnsOffset += TileSize;
		}
	}
}

void AMapManager::AttachComponents()
{
	ParentComp = CreateDefaultSubobject<USceneComponent>(TEXT("Parent"));
	SetRootComponent(ParentComp);

	WallTilesComponentInstance = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Wall Tiles"));
	WallTilesComponentInstance->AttachToComponent(ParentComp, FAttachmentTransformRules::KeepRelativeTransform);

	DestructibleTilesComponentInstance = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Destructible Tiles"));
	DestructibleTilesComponentInstance->AttachToComponent(ParentComp, FAttachmentTransformRules::KeepRelativeTransform);

	NonDestructibleTilesComponentInstance = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Non Destructible Tiles"));
	NonDestructibleTilesComponentInstance->AttachToComponent(ParentComp, FAttachmentTransformRules::KeepRelativeTransform);

	FloorTilesComponentInstance = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Floor Tiles"));
	FloorTilesComponentInstance->AttachToComponent(ParentComp, FAttachmentTransformRules::KeepRelativeTransform);

	WallTilesComponentInstance->SetCollisionResponseToChannel(TRACE_BOMB, ECR_Block);
	DestructibleTilesComponentInstance->SetCollisionResponseToChannel(TRACE_DESTRUCTIBLE_TILE, ECR_Block);
	NonDestructibleTilesComponentInstance->SetCollisionResponseToChannel(TRACE_BOMB, ECR_Block);
	FloorTilesComponentInstance->SetCollisionResponseToChannel(TRACE_FLOOR, ECR_Block);
}

int AMapManager::ColumnRowPosition(const int Column, const int Row) const
{
	return MapColumns * Row + Column;
}

void AMapManager::SetMap()
{
	CustomGameMode = Cast<ACustomGameMode>(GetWorld()->GetAuthGameMode());
	if (CustomGameMode) CustomGameMode->SetMapInstance(this);
}

FVector AMapManager::GetPlayerSpawnPosition(int Value)
{
	if (Value < PlayersSpawnTiles.Num()) return PlayersSpawnTiles[Value];
	return FVector::ZeroVector;
}

void AMapManager::DestroyTile(int TileInstance)
{
	if (TileInstance < 0) return;

	DestructibleTilesComponentInstance->RemoveInstance(TileInstance);
}

FVector AMapManager::GetTilePosition(int Instance, ETileType Type)
{
	FTransform tileTransform;

	switch (Type)
	{
	case ETileType::EWallTile: WallTilesComponentInstance->GetInstanceTransform(Instance, tileTransform, true);
		break;
	case ETileType::EDestructibleTile: DestructibleTilesComponentInstance->GetInstanceTransform(Instance, tileTransform, true);
		break;
	case ETileType::ENonDestructibleTile: NonDestructibleTilesComponentInstance->GetInstanceTransform(Instance, tileTransform, true);
		break;
	case ETileType::EFloorTile: FloorTilesComponentInstance->GetInstanceTransform(Instance, tileTransform, true);
		break;
	}

	return tileTransform.GetLocation();
}
