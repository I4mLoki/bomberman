#include "BombFactory.h"
#include "Bomb.h"

UBombFactory::UBombFactory()
{
	BombTemplate = nullptr;
	SpawnTransform = FTransform();
	WorldInstance = nullptr;
}

void UBombFactory::Init(TSubclassOf<ABomb> Bomb, const FTransform& InitialTransform, UWorld* WorldReference)
{
	BombTemplate = Bomb;
	SpawnTransform = InitialTransform;
	WorldInstance = WorldReference;
}

ABomb* UBombFactory::GetBomb()
{
	if (!BombTemplate)
		return nullptr;

	ABomb* Bomb = nullptr;

	if (BombsPool.IsEmpty())
	{
		Bomb = WorldInstance->SpawnActor<ABomb>(BombTemplate, SpawnTransform);
		Bomb->SetBombFactoryInstance(this);
	}
	else
		BombsPool.Dequeue(Bomb);
	
	return Bomb;
}

void UBombFactory::AddBombToPool(ABomb* Bomb)
{
	BombsPool.Enqueue(Bomb);
}
