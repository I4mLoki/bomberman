#include "GameCamera.h"
#include "CustomGameMode.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"

AGameCamera::AGameCamera()
{
	PrimaryActorTick.bCanEverTick = true;

	AttachComponents();
}

void AGameCamera::BeginPlay()
{
	Super::BeginPlay();
	
	CustomGameMode = Cast<ACustomGameMode>(GetWorld()->GetAuthGameMode());
}

void AGameCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	SpringArmComponentInstance->TargetArmLength = FMath::FInterpTo(SpringArmComponentInstance->TargetArmLength, ComputeTarget(), DeltaTime, ZoomSpeed);
	SetActorLocation(FMath::VInterpTo(GetActorLocation(), TargetPoint, DeltaTime, FollowSpeed));
}

void AGameCamera::AttachComponents()
{
	ParentComp = CreateDefaultSubobject<USceneComponent>(TEXT("Parent"));
	SetRootComponent(ParentComp);

	SpringArmComponentInstance = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	SpringArmComponentInstance->AttachToComponent(ParentComp, FAttachmentTransformRules::KeepRelativeTransform);

	CameraComponentInstance = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponentInstance->AttachToComponent(SpringArmComponentInstance, FAttachmentTransformRules::KeepRelativeTransform);

	SpringArmComponentInstance->TargetArmLength = MaxArmLenght + MinArmLenght / 2;
}

void AGameCamera::StartWorking()
{
	GetPlayers();
	SetAsMainCamera();

	int maxSize = CustomGameMode->GetMapInstance()->GetMapRows();
	MaxArmLenght = FMath::GetMappedRangeValueClamped(FVector2D(20, 40), MaxArmLenghtRange, maxSize);
}

void AGameCamera::GetPlayers()
{
	if (!CustomGameMode) CustomGameMode = Cast<ACustomGameMode>(GetWorld()->GetAuthGameMode());

	int TotalPlayers = CustomGameMode->GetNumPlayers();

	for (int i = 0; i < TotalPlayers; ++i)
	{
		APawn* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), i);
		if (Player) Players.Add(Player);
	}
}

void AGameCamera::SetAsMainCamera()
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);

	if (PlayerController) PlayerController->SetViewTarget(this);
}

float AGameCamera::ComputeTarget()
{
	float MaxDistance = 0;

	TargetPoint = GetActorLocation();

	for (int i = 0; i < Players.Num(); ++i)
	{
		for (int j = 1; j < Players.Num(); ++j)
		{
			if (i != j)
			{
				const float TempDistance = (Players[i]->GetActorLocation() - Players[j]->GetActorLocation()).Size();

				if (TempDistance > MaxDistance)
				{
					TargetPoint = (Players[i]->GetActorLocation() + Players[j]->GetActorLocation()) / 2;
					MaxDistance = TempDistance;
				}
			}
		}
	}

	MaxDistance += HeightOffset;
	return FMath::Clamp(MaxDistance, MinArmLenght, MaxArmLenght);
}
