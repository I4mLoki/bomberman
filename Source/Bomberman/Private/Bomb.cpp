#include "Bomb.h"

#include "BasePlayer.h"
#include "BombFactory.h"
#include "BombInterface.h"
#include "DestructibleTile.h"
#include "Bomberman/Bomberman.h"
#include "Kismet/GameplayStatics.h"

ABomb::ABomb()
{
	PrimaryActorTick.bCanEverTick = true;

	BombMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bomb Mesh"));
	BombMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void ABomb::BeginPlay()
{
	Super::BeginPlay();

	InitialBlastLong = BlastLong;
	CurrentBlastLong = InitialBlastLong;

	BombMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	BombMesh->SetVisibility(false, true);

	TriggerBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ABomb::Tick(float Delta)
{
	Super::Tick(Delta);
}

void ABomb::EnableBomb(bool Enable, bool TimedBomb)
{
	IsActive = Enable;
	BombMesh->SetVisibility(IsActive, true);

	if (IsActive)
	{
		TriggerBoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

		if (!TimedBomb)
			GetWorldTimerManager().SetTimer(TimeHandlerExplode, this, &ABomb::ExplodeBomb, TimeToExplode);
	}
	else
	{
		TriggerBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		CurrentBlastLong = InitialBlastLong;
		BombOwner = nullptr;

		if (BombFactoryInstance)
			BombFactoryInstance->AddBombToPool(this);
	}

	EnableBombEvent(Enable);
}

void ABomb::SetBombFactoryInstance(UBombFactory* BombFactory)
{
	BombFactoryInstance = BombFactory;
}

void ABomb::ExplodeBomb()
{
	if (IsExploding || !IsActive) return;

	IsExploding = true;

	TraceBombBlast(GetActorForwardVector());
	TraceBombBlast(GetActorForwardVector() * - 1);
	TraceBombBlast(GetActorRightVector());
	TraceBombBlast(GetActorRightVector() * -1);

	IBombInterface* BombInterface = Cast<IBombInterface>(BombOwner);

	if (BombOwner && BombInterface) BombInterface->Execute_BombExplode(BombOwner, this, Points);

	if (BombInterface && KilledPlayers.Num() > 0) BombInterface->Execute_PlayersKilled(BombOwner, KilledPlayers);

	EnableBomb(false);
	IsExploding = false;
	Points = 0;
	KilledPlayers.Reset();
}

void ABomb::SetBombData(AActor* BOwner, float Size)
{
	BombOwner = BOwner;
	TileSize = Size;
}

void ABomb::AddBlastSize(int Size)
{
	CurrentBlastLong += Size;
}

void ABomb::TraceBombBlast(FVector Direction)
{
	TArray<FHitResult> Hits;

	FCollisionQueryParams TraceParams;
	TraceParams.AddIgnoredActor(this);

	GetWorld()->LineTraceMultiByChannel(Hits, GetActorLocation(), GetActorLocation() + Direction * CurrentBlastLong * TileSize, TRACE_BOMB, TraceParams);

	int totalblast = CurrentBlastLong;

	for (int i = 0; i < Hits.Num(); ++i)
	{
		if (!Hits[i].bBlockingHit)
		{
			ADestructibleParent* Destructible = Cast<ADestructibleParent>(Hits[i].GetActor());
			ABasePlayer* Player = Cast<ABasePlayer>(Hits[i].GetActor());

			if (Destructible)
			{
				ADestructibleTile* DestructibleTile = Cast<ADestructibleTile>(Destructible);

				if (DestructibleTile)
					Points += PointsPerDestructibleTile;

				Destructible->DestroyObject();
			}

			if (Player)
			{
				if (BombOwner != Hits[i].GetActor()) Points += PointsPerPlayerKill;
				if (!KilledPlayers.Contains(Player)) KilledPlayers.Add(Player);
			}
		}
		else
			totalblast = (Hits[i].ImpactPoint - GetActorLocation()).Size() / TileSize;
	}

	FVector BombFXLocation = GetActorLocation();
	for (int i = 0; i < totalblast; ++i)
	{
		BombFXLocation += Direction * TileSize;
		if (BombFX) UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BombFX, FTransform(BombFXLocation));
	}
}

void ABomb::DestroyObject()
{
	if (IsExploding) return;

	GetWorldTimerManager().ClearTimer(TimeHandlerExplode);
	ExplodeBomb();
}
