#include "BasePlayer.h"

#include "Bomb.h"
#include "Bomberman/Bomberman.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CustomGameMode.h"
#include "DrawDebugHelpers.h"

ABasePlayer::ABasePlayer()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	bUseControllerRotationYaw = false;
}

void ABasePlayer::BeginPlay()
{
	Super::BeginPlay();

	GetCapsuleComponent()->SetCollisionObjectType(TRACE_PLAYER);
	GetCapsuleComponent()->SetCollisionResponseToChannel(TRACE_BOOSTER, ECR_Overlap);
	GetCapsuleComponent()->SetCollisionResponseToChannel(TRACE_BOMB, ECR_Overlap);

	CustomGameModeInstance = Cast<ACustomGameMode>(GetWorld()->GetAuthGameMode());

	CurrentBombs = InitialBombs;
	MaxBombs = CurrentBombs;
	InitialSpeed = GetCharacterMovement()->MaxWalkSpeed;
	GameTime = CustomGameModeInstance->GameTime;
}

void ABasePlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABasePlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	APlayerController* PlayerController = Cast<APlayerController>(GetController());

	if (PlayerController)
	{
		switch (PlayerController->GetLocalPlayer()->GetControllerId())
		{
		case 0:
			{
				PlayerInputComponent->BindAxis("P1_MoveForward", this, &ABasePlayer::MoveForward);
				PlayerInputComponent->BindAxis("P1_MoveRight", this, &ABasePlayer::MoveRight);
				PlayerInputComponent->BindAction("P1_PlaceBomb", IE_Pressed, this, &ABasePlayer::PlaceBomb);
				break;
			}
		case 1:
			{
				PlayerInputComponent->BindAxis("P2_MoveForward", this, &ABasePlayer::MoveForward);
				PlayerInputComponent->BindAxis("P2_MoveRight", this, &ABasePlayer::MoveRight);
				PlayerInputComponent->BindAction("P2_PlaceBomb", IE_Pressed, this, &ABasePlayer::PlaceBomb);
				break;
			}
		}
	}
}

void ABasePlayer::BombExplode_Implementation(AActor* Bomb, int Score)
{
	CurrentBombs = FMath::Clamp(CurrentBombs + 1, 0, MaxBombs);

	FString BombText = "Bombs = " + FString::FromInt(CurrentBombs);
	UpdateGameplayUI.Broadcast(EPlayerProperty::EMoreBombs, BombText);

	CurrentScore += Score;

	if (CurrentScore > HighestScore)
	{
		HighestScore = CurrentScore;
		UpdateGameplayUI.Broadcast(EPlayerProperty::EHighestScore, FString::FromInt(HighestScore));
	}

	UpdateGameplayUI.Broadcast(EPlayerProperty::ECurrentScore, FString::FromInt(CurrentScore));
}

void ABasePlayer::PlayersKilled_Implementation(TArray<AActor*>& KilledPlayers)
{
	TArray<int> KilledPlayersId;

	for (int i = 0; i < KilledPlayers.Num(); ++i)
	{
		ABasePlayer* Player = Cast<ABasePlayer>(KilledPlayers[i]);

		if (!Player) continue;

		APlayerController* PlayerController = Cast<APlayerController>(Player->GetController());
		KilledPlayersId.Add(PlayerController->GetLocalPlayer()->GetControllerId());
	}

	CustomGameModeInstance->PlayersDead(KilledPlayersId);
}

void ABasePlayer::MoreBombsBooster_Implementation(int Amount)
{
	MaxBombs += Amount;
	CurrentBombs = FMath::Clamp(CurrentBombs + Amount, 0, MaxBombs);

	FString BombText = "Bombs = " + FString::FromInt(CurrentBombs);
	UpdateGameplayUI.Broadcast(EPlayerProperty::EMoreBombs, BombText);
}

void ABasePlayer::BombRangeBooster_Implementation(int Amount)
{
	ExtraBlastSize += Amount;

	FString BombRangeText = "Extra Blast Size = " + FString::FromInt(ExtraBlastSize);
	UpdateGameplayUI.Broadcast(EPlayerProperty::EBombsRange, BombRangeText);
}

void ABasePlayer::SpeedUpBooster_Implementation(float Amount)
{
	GetCharacterMovement()->MaxWalkSpeed += Amount;

	FString SpeedText = "Speed = " + FString::FromInt(GetCharacterMovement()->MaxWalkSpeed);
	UpdateGameplayUI.Broadcast(EPlayerProperty::ESpeedUp, SpeedText);
}

void ABasePlayer::MoreRemoteBombsBooster_Implementation(float BoosterTime)
{
	RemoteBombs = true;
	GetWorldTimerManager().SetTimer(TimeHandleRemoteBombs, this, &ABasePlayer::DisableRemoteBombBooster, BoosterTime);

	FString RemoteBombText = "Remote bomb? Yes";
	UpdateGameplayUI.Broadcast(EPlayerProperty::EMoreRemoteBombs, RemoteBombText);
}

void ABasePlayer::SetPlayerInputEnabled(bool Enabled)
{
	AcceptInput = Enabled;
}

void ABasePlayer::MoveForward(const float Value)
{
	if (!AcceptInput) return;

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void ABasePlayer::MoveRight(const float Value)
{
	if (!AcceptInput) return;

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}

void ABasePlayer::PlaceBomb()
{
	if (!CustomGameModeInstance || !AcceptInput) return;

	if (RemoteBombs && CurrentTimedBomb)
	{
		DisableRemoteBombBooster();
		return;
	}

	if (CurrentBombs <= 0) return;

	ABomb* Bomb = CustomGameModeInstance->CreateBomb(this);

	if (!Bomb) return;

	Bomb->SetActorLocation(GetTileCenterPosition(), false, nullptr, ETeleportType::TeleportPhysics);
	Bomb->AddBlastSize(ExtraBlastSize);
	Bomb->EnableBomb(true, RemoteBombs);

	CurrentTimedBomb = RemoteBombs ? Bomb : nullptr;
	CurrentBombs = FMath::Clamp(CurrentBombs - 1, 0, MaxBombs);

	FString BombText = "Bombs = " + FString::FromInt(CurrentBombs);;
	UpdateGameplayUI.Broadcast(EPlayerProperty::EMoreBombs, BombText);
}

void ABasePlayer::DisableRemoteBombBooster()
{
	RemoteBombs = false;

	if (!CurrentTimedBomb) return;

	CurrentTimedBomb->ExplodeBomb();
	CurrentTimedBomb = nullptr;

	FString RemoteBombText = "Remote bomb? No";
	UpdateGameplayUI.Broadcast(EPlayerProperty::EMoreRemoteBombs, RemoteBombText);
}

FVector ABasePlayer::GetTileCenterPosition()
{
	FVector Pos = GetActorLocation();
	FHitResult Hit(ForceInit);

	if (GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation(), GetActorLocation() - GetActorUpVector() * 500.f, TRACE_FLOOR))
	{
		if (CustomGameModeInstance)
		{
			Pos = CustomGameModeInstance->GetMapTilePosition(Hit.Item, ETileType::EFloorTile);
			Pos.Z += CustomGameModeInstance->GetMapTileSize();
		}
	}

	return Pos;
}
