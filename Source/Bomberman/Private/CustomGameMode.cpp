#include "CustomGameMode.h"

#include "Bomb.h"
#include "BombFactory.h"

ACustomGameMode::ACustomGameMode()
{
	bStartPlayersAsSpectators = true;
}

void ACustomGameMode::BeginPlay()
{
	Super::BeginPlay();

	GameStarted();
}

void ACustomGameMode::GameStarted_Implementation()
{
	GetWorldTimerManager().SetTimer(GameTimeTimerHandle, this, &ACustomGameMode::GameTimeOver, GameTime);
}

ABomb* ACustomGameMode::CreateBomb(AActor* BombOwner)
{
	if (!BombFactory)
	{
		BombFactory = NewObject<UBombFactory>();
		BombFactory->Init(BombTemplate, MapInstance->GetActorTransform(), GetWorld());
	}

	ABomb* Bomb = BombFactory->GetBomb();

	if (Bomb) Bomb->SetBombData(BombOwner, MapInstance->GetMapTileSize());

	return Bomb;
}

void ACustomGameMode::SetChronoEnabled(bool Enabled)
{
	if (!Enabled)
		GetWorldTimerManager().ClearTimer(GameTimeTimerHandle);
	else
		GetWorldTimerManager().SetTimer(GameTimeTimerHandle, this, &ACustomGameMode::GameTimeOver, GameTime);
}

void ACustomGameMode::SetMapInstance(AMapManager* MapManager)
{
	MapInstance = MapManager;
	MapReady();
}

FVector ACustomGameMode::GetMapTilePosition(int Instance, ETileType Type)
{
	return MapInstance ? MapInstance->GetTilePosition(Instance, Type) : FVector::ZeroVector;
}

float ACustomGameMode::GetMapTileSize()
{
	return MapInstance ? MapInstance->GetMapTileSize() : 0;
}
