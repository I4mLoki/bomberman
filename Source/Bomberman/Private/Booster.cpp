#include "Booster.h"

#include "BoosterInterface.h"
#include "Bomberman/Bomberman.h"
#include "Kismet/GameplayStatics.h"

ABooster::ABooster()
{
	PrimaryActorTick.bCanEverTick = true;
	AttachComponents();
}

void ABooster::BeginPlay()
{
	Super::BeginPlay();

	BoosterMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	TriggerBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ABooster::OnOverlapBegin);
	TriggerBoxComponent->SetCollisionObjectType(TRACE_BOOSTER);
	TriggerBoxComponent->SetCollisionResponseToChannel(TRACE_PLAYER, ECR_Overlap);
}

void ABooster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABooster::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult)
{
	if (HasBeenPicked) return;

	IBoosterInterface* BoosterInterface = Cast<IBoosterInterface>(OtherActor);
	if (!BoosterInterface) return;
	
	HasBeenPicked = true;
	
	PickupBoosterAction(OtherActor);
	SetLifeSpan(0.1f);
}

void ABooster::AttachComponents()
{
	BoosterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Booster Mesh"));
	BoosterMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}
