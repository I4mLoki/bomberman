#include "DestructibleTile.h"
#include "Booster.h"
#include "CustomUtils.h"
#include "Bomberman/Bomberman.h"

void ADestructibleTile::DestroyObject()
{
	if (BoostersToSpawn.Num() > 0)
	{
		if (UCustomUtils::CheckProbability(BoosterChance))
		{
			int Index = FMath::RandRange(0, BoostersToSpawn.Num() - 1);
			GetWorld()->SpawnActor<ABooster>(BoostersToSpawn[Index].Get(), GetActorTransform());
		}
	}
	
	FHitResult Hit;
	FCollisionQueryParams TraceParams;
	TraceParams.AddIgnoredActor(this);

	if (GetWorld()->LineTraceSingleByChannel(Hit, GetActorLocation() + GetActorUpVector() * 200.f, GetActorLocation() - GetActorUpVector() * 200.f, TRACE_DESTRUCTIBLE_TILE, TraceParams))
		if (MapInstance) MapInstance->DestroyTile(Hit.Item);

	Super::DestroyObject();
}
