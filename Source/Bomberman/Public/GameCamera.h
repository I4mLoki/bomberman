#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameCamera.generated.h"

UCLASS()
class BOMBERMAN_API AGameCamera : public AActor
{
	GENERATED_BODY()

public:
	AGameCamera();
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category ="Game Camera")
	void StartWorking();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Camera")
	float MinArmLenght = 800.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Camera")
	float MaxArmLenght = 2000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Camera")
	float ZoomSpeed = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Camera")
	float FollowSpeed = 2.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Camera")
	float HeightOffset = 200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Camera")
	FVector2D MaxArmLenghtRange = FVector2D(1800, 3700);

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USceneComponent* ParentComp;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	class USpringArmComponent* SpringArmComponentInstance;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	class UCameraComponent* CameraComponentInstance;

private:
	void AttachComponents();
	void GetPlayers();
	void SetAsMainCamera();
	float ComputeTarget();

	class ACustomGameMode* CustomGameMode;
	FVector TargetPoint;
	TArray<APawn*> Players;
	FTimerHandle TimerHandle;
};
