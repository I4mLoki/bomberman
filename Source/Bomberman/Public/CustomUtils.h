#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CustomUtils.generated.h"

UCLASS()
class BOMBERMAN_API UCustomUtils : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Custom Utils")
	static bool CheckProbability(float Chance);
};
