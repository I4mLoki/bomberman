#pragma once

#include "CoreMinimal.h"

#include "BasePlayer.h"
#include "DestructibleParent.h"
#include "Bomb.generated.h"

UCLASS()
class BOMBERMAN_API ABomb : public ADestructibleParent
{
	GENERATED_BODY()

public:
	ABomb();

	virtual void BeginPlay() override;
	virtual void Tick(float Delta) override;
	virtual void DestroyObject() override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Bomb")
	void EnableBombEvent(bool Enabled);

	void EnableBomb(bool Enable, bool TimedBomb = false);
	void SetBombFactoryInstance(class UBombFactory* BombFactory);
	void ExplodeBomb();
	void SetBombData(AActor* BOwner, float Size);
	void AddBlastSize(int Size);

	int InitialBlastLong;

protected:
	UPROPERTY(EditDefaultsOnly, Category ="Bomb Component")
	UStaticMeshComponent* BombMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Bomb")
	float TimeToExplode = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = "Bomb")
	int BlastLong = 3;

	UPROPERTY(EditDefaultsOnly, Category = "Bomb")
	UParticleSystem* BombFX;

	UPROPERTY(EditDefaultsOnly, Category = "Bomb")
	int PointsPerDestructibleTile = 10;

	UPROPERTY(EditDefaultsOnly, Category = "Bomb")
	int PointsPerPlayerKill = 100;

	void TraceBombBlast(FVector Direction);
private:
	float TileSize = 50.f;
	bool IsActive = false;
	AActor* BombOwner = nullptr;
	bool IsExploding = false;
	int CurrentBlastLong;
	UBombFactory* BombFactoryInstance;
	FTimerHandle TimeHandlerExplode;
	int Points;
	TArray<AActor*> KilledPlayers;
};
