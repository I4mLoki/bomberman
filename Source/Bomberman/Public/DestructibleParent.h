#pragma once

#include "CoreMinimal.h"
#include "MapManager.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "DestructibleParent.generated.h"

UCLASS()
class BOMBERMAN_API ADestructibleParent : public AActor
{
	GENERATED_BODY()

public:
	ADestructibleParent();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Destructible Object")
	virtual void DestroyObject();

	void SetData(float Size, AMapManager* MapManager = nullptr);

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Component")
	UBoxComponent* TriggerBoxComponent;

	float BoxSize = 40.f;
	AMapManager* MapInstance;
};
