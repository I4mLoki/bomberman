#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BoosterInterface.generated.h"

UINTERFACE(Blueprintable, MinimalAPI)
class UBoosterInterface : public UInterface
{
	GENERATED_BODY()
};

class BOMBERMAN_API IBoosterInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster")
	void MoreBombsBooster(int Amount = 1);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster")
	void BombRangeBooster(int Amount = 1);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster")
	void SpeedUpBooster(float Amount = 100.f);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster")
	void MoreRemoteBombsBooster(float BoosterTime = 10.f);
};
