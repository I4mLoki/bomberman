#pragma once

#include "CoreMinimal.h"
#include "DestructibleParent.h"
#include "DestructibleTile.generated.h"

UCLASS()
class BOMBERMAN_API ADestructibleTile : public ADestructibleParent
{
	GENERATED_BODY()

public:
	virtual void DestroyObject() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Destructible Tile")
	float BoosterChance = 0.3f;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Destructible Tile")
	TArray<TSubclassOf<class ABooster>> BoostersToSpawn;
};
