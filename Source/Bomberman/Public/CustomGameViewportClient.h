#pragma once

#include "CoreMinimal.h"
#include "Engine/GameViewportClient.h"
#include "CustomGameViewportClient.generated.h"

UCLASS()
class BOMBERMAN_API UCustomGameViewportClient : public UGameViewportClient
{
	GENERATED_BODY()

	virtual bool InputKey(const FInputKeyEventArgs& EventArgs) override;
};
