#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MapManager.generated.h"

UENUM(BlueprintType)
enum class ETileType : uint8
{
	EEmptyTile = 0 UMETA(DisplayName = "Empty Tile"),
	EWallTile = 1 UMETA(DisplayName = "Wall Tile"),
	EDestructibleTile = 2 UMETA(DisplayName = "Destructible Tile"),
	ENonDestructibleTile = 3 UMETA(DisplayName = "Non Destructible Tile"),
	EPlayerTile = 4 UMETA(DisplayName = "Player Tile"),
	EFloorTile = 5 UMETA(DisplayName = "Floor Tile")
};

USTRUCT()
struct FTile
{
	GENERATED_BODY()

	UPROPERTY(SaveGame)
	FVector TilePosition;

	UPROPERTY(SaveGame)
	ETileType TileType;

	UPROPERTY(SaveGame)
	int MeshInstance;

	FTile()
	{
		TilePosition = FVector::ZeroVector;
		TileType = ETileType::EEmptyTile;
		MeshInstance = -1;
	}
};

UCLASS()
class BOMBERMAN_API AMapManager final : public AActor
{
	GENERATED_BODY()

public:
	AMapManager();

	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Map Manager")
	FVector GetPlayerSpawnPosition(int Value);
	void DestroyTile(int TileInstance);
	FVector GetTilePosition(int Instance, ETileType Type);

	FORCEINLINE int GetMapColumns() { return MapColumns; }
	FORCEINLINE int GetMapRows() { return MapRows; }
	FORCEINLINE float GetMapTileSize() { return TileSize; }

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Map Manager")
	int MapColumns = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Map Manager")
	int MapRows = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Manager")
	float SpawnDestructibleTileChance = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Manager")
	float SpawnNonDestructibleTileChance = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Map Manager")
	UStaticMesh* WallTileMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Map Manager")
	UStaticMesh* DestructibleTileMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Map Manager")
	UStaticMesh* NonDestructibleTileMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Map Manager")
	UStaticMesh* FloorTileMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Manager")
	TSubclassOf<class ADestructibleTile> DestructibleTileTemplate;

private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USceneComponent* ParentComp;

	UPROPERTY(EditDefaultsOnly, Category="Components")
	UInstancedStaticMeshComponent* WallTilesComponentInstance;

	UPROPERTY(EditDefaultsOnly, Category="Components")
	UInstancedStaticMeshComponent* DestructibleTilesComponentInstance;

	UPROPERTY(EditDefaultsOnly, Category="Components")
	UInstancedStaticMeshComponent* NonDestructibleTilesComponentInstance;

	UPROPERTY(EditDefaultsOnly, Category="Components")
	UInstancedStaticMeshComponent* FloorTilesComponentInstance;

	void PopulateMap();
	void SetMapMeshes();
	void AttachComponents();
	void SetMap();
	int ColumnRowPosition(int Column, int Row) const;
	void DeleteTilesNextToPlayer(int Column, int Row);
	void CreateDestructibleTiles();

	TArray<FTile> TileList;
	TArray<FVector> PlayersSpawnTiles;
	float TileSize;
	class ACustomGameMode* CustomGameMode;
};
