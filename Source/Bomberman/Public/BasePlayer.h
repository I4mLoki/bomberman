#pragma once

#include "CoreMinimal.h"

#include "BombInterface.h"
#include "BoosterInterface.h"
#include "GameFramework/Character.h"
#include "BasePlayer.generated.h"

UENUM(BlueprintType)
enum class EPlayerProperty : uint8
{
	EMoreBombs = 0 UMETA(DisplayName = "More Bombs Booster"),
	EBombsRange = 1 UMETA(DisplayName = "Bomb Range Booster"),
	ECurrentScore = 2 UMETA(DisplayName = "Current Game Score"),
	ESpeedUp = 3 UMETA(DisplayName = "Speed Up Booster"),
	EMoreRemoteBombs = 4 UMETA(DisplayName = "More Remote Bombs Booster"),
	EHighestScore = 5 UMETA(DisplayName = "Highest Score")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FUpdateGameplayUI, EPlayerProperty, PlayerProperty, FString, Value);

UCLASS()
class BOMBERMAN_API ABasePlayer : public ACharacter, public IBombInterface, public IBoosterInterface
{
	GENERATED_BODY()

public:
	ABasePlayer();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Bomb Interface")
	void BombExplode(AActor* Bomb, int Score);
	virtual void BombExplode_Implementation(AActor* Bomb, int Score) override;

	virtual void PlayersKilled_Implementation(TArray<AActor*>& KilledPlayers) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster Interface")
	void MoreBombsBooster(int Amount);
	virtual void MoreBombsBooster_Implementation(int Amount) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster Interface")
	void BombRangeBooster(int Amount);
	virtual void BombRangeBooster_Implementation(int Amount) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster Interface")
	void SpeedUpBooster(float Amount);
	virtual void SpeedUpBooster_Implementation(float Amount) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Booster Interface")
	void MoreRemoteBombsBooster(float BoosterTime);
	virtual void MoreRemoteBombsBooster_Implementation(float BoosterTime) override;

	UFUNCTION(BlueprintCallable, Category = "Player")
	void SetPlayerInputEnabled(bool Enabled);

	UPROPERTY(BlueprintAssignable, Category = "Gameplay Event")
	FUpdateGameplayUI UpdateGameplayUI;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	FString Winner;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	int CurrentScore;

	UPROPERTY(BlueprintReadWrite, Category = "Player")
	int HighestScore;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	int InitialBombs = 1;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	float InitialSpeed;

	UPROPERTY(BlueprintReadOnly, Category = "Player")
	float GameTime;

protected:
	virtual void BeginPlay() override;

private:
	void MoveForward(float Value);
	void MoveRight(float Value);
	void PlaceBomb();
	void DisableRemoteBombBooster();

	FVector GetTileCenterPosition();

	class ACustomGameMode* CustomGameModeInstance;
	int MaxBombs;
	int CurrentBombs;
	class ABomb* CurrentTimedBomb;
	bool RemoteBombs = false;
	FTimerHandle TimeHandleRemoteBombs;
	int ExtraBlastSize;
	bool AcceptInput;
};
