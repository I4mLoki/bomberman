#pragma once

#include "CoreMinimal.h"
#include "BombFactory.generated.h"

UCLASS()
class BOMBERMAN_API UBombFactory : public UObject
{
	GENERATED_BODY()

public:
	UBombFactory();

	void Init(TSubclassOf<class ABomb> Bomb, const FTransform& InitialTransform, UWorld* WorldReference);
	class ABomb* GetBomb();
	void AddBombToPool(class ABomb* Bomb);

private:
	TQueue<class ABomb*> BombsPool;
	TSubclassOf<class ABomb> BombTemplate;
	FTransform SpawnTransform;
	UWorld* WorldInstance;
};
