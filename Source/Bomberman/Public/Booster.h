#pragma once

#include "CoreMinimal.h"
#include "DestructibleParent.h"
#include "Booster.generated.h"

UCLASS()
class BOMBERMAN_API ABooster : public ADestructibleParent
{
	GENERATED_BODY()

public:
	ABooster();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Pickup Booster Action")
	void PickupBoosterAction(AActor* Actor);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UStaticMeshComponent* BoosterMesh;

private:
	void AttachComponents();

	bool HasBeenPicked;
};
