#pragma once

#include "CoreMinimal.h"
#include "BombInterface.generated.h"

UINTERFACE(Blueprintable, MinimalAPI)
class UBombInterface : public UInterface
{
	GENERATED_BODY()
};

class BOMBERMAN_API IBombInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Bomb Interface")
	void BombExplode(AActor* Bomb, int Score);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Bomb Interface")
	void PlayersKilled(TArray<AActor*>& KilledPlayers);
};
