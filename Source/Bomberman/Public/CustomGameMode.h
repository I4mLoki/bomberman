#pragma once

#include "CoreMinimal.h"

#include "MapManager.h"
#include "GameFramework/GameModeBase.h"
#include "CustomGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API ACustomGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACustomGameMode();

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category="Custom Game Mode")
	void MapReady();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="Custom Game Mode")
	void GameStarted();

	UFUNCTION(BlueprintCallable, Category = "Custom Game Mode")
	class ABomb* CreateBomb(AActor* BombOwner);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Custom Game Mode")
	void PlayersDead(const TArray<int>& PlayerIds);

	UFUNCTION(BlueprintCallable, Category = "Custom Game Mode")
	void SetChronoEnabled(bool Enabled);

	void SetMapInstance(AMapManager* MapManager);
	FVector GetMapTilePosition(int Instance, ETileType Type);
	float GetMapTileSize();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Custom Game Mode")
	float GameTime = 60.f;

	FORCEINLINE class AMapManager* GetMapInstance() { return MapInstance; }

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Custom Game Mode")
	class AMapManager* MapInstance;

	UPROPERTY(BlueprintReadWrite, Category = "Custom Game Mode")
	TArray<class ABasePlayer*> Players;

	UPROPERTY(EditDefaultsOnly, Category = "Custom Game Mode")
	TSubclassOf<class ABomb> BombTemplate;

	UPROPERTY()
	class UBombFactory* BombFactory;

	UPROPERTY(BlueprintReadWrite, Category = "Custom Game Mode")
	FTimerHandle GameTimeTimerHandle;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Custom Game Mode")
	void GameTimeOver();
};
