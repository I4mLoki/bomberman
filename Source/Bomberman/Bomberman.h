// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define TRACE_PLAYER ECC_GameTraceChannel1
#define TRACE_BOOSTER ECC_GameTraceChannel2
#define TRACE_DESTRUCTIBLE_TILE ECC_GameTraceChannel3
#define TRACE_BOMB ECC_GameTraceChannel4
#define TRACE_FLOOR ECC_GameTraceChannel5