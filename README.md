# Bomberman

This game is a replica of the classic Bomberman.

## Controls

- First player: 
  - Left - A
  - Up - W
  - Down - S
  - Right - D
  - Place Bomb - Space Bar
  </br></br>
- Second player:
  - Left - Left Arrow
  - Up - Up Arrow
  - Down - Down Arrow
  - Right - Right Arrow
  - Place Bomb - Right CTRL

## Features

- Static top-down view on the level
- Camera that pans in/out depending on distance between players in the level
- Procedural generated maps
- Two players play on one machine using different keys to control two characters
- Implementation of different pickups
  - Longer bomb blasts (Pink boosters)
  - More bombs (Yellow boosters)
  - Faster run speed (Blue boosters)
  - Remote-controlled bombs (Red boosters)
- Bomb placing by the player
  - Player is starting with only one bomb that can be active at a time
  - Placing a bomb subtracts one from the count, when the bomb explodes the count goes up again
  - Amount is upgradable with pickups
  - Once the remote detonator has been picked up only one bomb can be active until the power-up runs out
- Player death when standing in bomb blast
- Bomb blasts
  - Linear in four main directions
  - Can penetrate players/pickups when going off (killing/destroying them)
  - Are stopped by walls
  - Trigger other bombs
- Differentiation between destructible and indestructible walls, destructible walls can spawn random pickups upon destruction
- Win conditions
  - Show win screen when only one player is alive
  - Show a map timer, that counts down and ends the round
  - Show draw when the last players die in the same bomb blast or multiple players are alive when the timer runs out
  - After round end, freeze game in its current state
- Reset option on end screen
  - Starts another round
  - Saves previous score

## Next steps

- Improve user interface
- Clean up blueprints and tidy them up
- AI enemies that behave like a player
- Implement music and sound effects
- Add more boosters
- Make it multiplayer online
- Creating a main menu and sound control options
- Different levels of difficulty

## Time spent in development - 24h